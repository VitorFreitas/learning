function Geo(){
	var geocoder  = new google.maps.Geocoder();
	
	this.callGeoApi = function(callback){
		if (navigator.geolocation) {
		    navigator.geolocation.getCurrentPosition(callback, this.errorFunction);
		};	 
	}

	this.errorFunction = function (e){
	    alert("Erro na GeoLocalizacao. Seu browser pode ser antigo para a aplicacao ou voce pode ter negado o acesso a Localização : " + e.message);    
	}
  
  	this.getMapResults = function (callback){
  		this.callGeoApi(function(position){  			
			  	var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);			  
			    geocoder.geocode({'location': latlng}, callback);
  		})  		  		
  	}

  this.locate = function (type , callback) {
    this.getMapResults(function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {    
        if (results[0]) {                  
             for (var i=0; i<results[0].address_components.length; i++) {
	            
	            for (var b=0;b<results[0].address_components[i].types.length;b++) {

	                if (results[0].address_components[i].types[b] == getGoogleValueForType(type)) {	                    
	                	callback(results[0].address_components[i]);
	                }
	            }
        }
        
        } else {
          callback(null);
        }
      } else {
        callback(null);
      }
    });
	}

	function getGoogleValueForType (type) {
		var googleValues = {"city" : {gvalue : "administrative_area_level_1"}};
		for(var t in googleValues) { 
			if (t == type) {				
				return googleValues[t].gvalue;
			}
		}
	}
}