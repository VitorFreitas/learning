var mongo = require('mongodb')

var db = mongo.Db , con = mongo.Connection , server = mongo.Server

BlogPost = function(host , port) {
	this.db = new db('blogpost' , new server(host , port , {safe : false} , {auto_reconnect : true} , {}));
	this.db.open(function(){});
}

BlogPost.prototype.getCollection = function(callback) {
    this.db.collection('posts' , function(error , postCollection){
		if (error) {
			throw "Unable to connect with posts collection"
		} else {
			callback(postCollection);
		}
	});
}



BlogPost.prototype.all = function (callback) {
		this.getCollection(function(collection){ 
							collection.find().toArray(function (e , results) {
									if (e) throw "Unable to query on posts"
									else callback(results)									
								});
						});
	}

BlogPost.prototype.save = function(post , callback) {
		this.getCollection(function(collection){ 
				post.dateCreated = new Date();
				collection.insert(post , function(){					
						callback(post);
					});
		});
}

exports.BlogPost = BlogPost