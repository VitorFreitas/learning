var express = require('express')
  , routes = require('./routes')
  , user = require('./routes/user')
  , http = require('http')
  , path = require('path')
  , blogPost = require('./blogpost').BlogPost;

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(require('stylus').middleware(__dirname + '/public'));
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

var post = new BlogPost('localhost' , 27017)

app.get('/', function(req , res) {
		post.all(function(results){
				res.render('index' , {
						title : "aeeeee" , posts : results
					});
			});
	});

app.get('/post/save' , function(req , res) {
	res.render('post_save' , {title : 'Save post'});
});

app.post('/post/save' , function(req , res) {
	var postToBeSaved = {title : req.param('title') , text : req.param('text') , lat : req.param('lat') , lon : req.param('lon') , info : req.param('weather')}
	post.save(postToBeSaved  , function(result){
		res.redirect('/')
	})
});	

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});