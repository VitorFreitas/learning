package com.scala

import scala.reflect.If

object worksheettest {
  
	
		def abs(x : Double) : Double = if ( x < 0 ) -x else x
                                                  //> abs: (x: Double)Double
		
		def sqrtItr (guess : Double , x : Double) : Double =
		if(isGoodEnough(guess , x)) guess
		else sqrtItr(improve(guess , x), x)
                                                  //> sqrtItr: (guess: Double, x: Double)Double
		
		def isGoodEnough (guess : Double , x :Double) = abs(guess * guess - x) < 0.001
                                                  //> isGoodEnough: (guess: Double, x: Double)Boolean
		
		def improve(guess : Double , x : Double) = (guess + x / guess) / 2
                                                  //> improve: (guess: Double, x: Double)Double

		var eval = sqrtItr(1, 4)          //> eval  : Double = 2.0000000929222947
		
		
		
}