package com.scala

import scala.reflect.If

object worksheettest {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(127); 
  
	
		def abs(x : Double) : Double = if ( x < 0 ) -x else x;System.out.println("""abs: (x: Double)Double""");$skip(132); 
		
		def sqrtItr (guess : Double , x : Double) : Double =
		if(isGoodEnough(guess , x)) guess
		else sqrtItr(improve(guess , x), x);System.out.println("""sqrtItr: (guess: Double, x: Double)Double""");$skip(84); 
		
		def isGoodEnough (guess : Double , x :Double) = abs(guess * guess - x) < 0.001;System.out.println("""isGoodEnough: (guess: Double, x: Double)Boolean""");$skip(72); 
		
		def improve(guess : Double , x : Double) = (guess + x / guess) / 2;System.out.println("""improve: (guess: Double, x: Double)Double""");$skip(28); 

		var eval = sqrtItr(1, 4);System.out.println("""eval  : Double = """ + $show(eval ))}
		
		
		
}