-module(shop1).
-export([total/1]).

total([{What, N}|T]) -> cost(What) * N + total(T);
total([]) -> 0.

cost(uva) -> 10;
cost(pera) -> 20;
cost(maca) -> 30;
cost(saladamista) -> 40.
