-module(geometry).
-export([area/1]).
area({rec, Wid, Ht }) -> Wid * Ht;
area({circle , R}) -> 3.14159 * R * R.
