
def mapStrings(args):
	""" A function that returns a  json map """
	return ",".join(["{%s : %s}" % (k,v) for k,v in args.items()])
	

if __name__ == "__main__":
	print "Doc : "  + mapStrings.__doc__ 
	print mapStrings({"yeaah2" : "1" , "yeaah" : "2"})